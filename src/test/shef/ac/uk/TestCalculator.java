package test.shef.ac.uk;

import org.junit.Test;

import shef.ac.uk.Calculator;

import junit.framework.TestCase;

public class TestCalculator extends TestCase
{
	@Test
	public void testAdd() {
		assertEquals(Calculator.add(2,1), 3);
	}

	@Test
	public void testSub() {
		assertEquals(Calculator.sub(2,1), 1);
	}

	@Test
	public void testMul1() {
		assertEquals(Calculator.mul(2,2), 4);
	}

	@Test
	public void testMul2() {
		assertEquals(Calculator.mul(3,2), 6);
	}

	@Test
	public void testDiv() {
		assertEquals(Calculator.div(2,1), 2);
	}
}
